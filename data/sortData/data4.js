function filterData4(data4) {
    // variabel untuk menampung hasil
    const result = [];
    // variabel untuk menampung object
    let dataValue = 0
    // variabel untuk menyimpan apabila data tidak ditemukan
    let empty = 0

    for(let i = 0; i<data4.length; i++){
        // mengambil data yang pelangi atau intel dan warna matanya harus hijau
        if((data4[i].company === "Pelangi" || data4[i].company === "Intel") && data4[i].eyeColor === "green"){
            result[dataValue] = data4[i]
            dataValue++
        }
        // if(!result.length){
        //     notFound = "message : data tidak ada/tidak di temukan"
        //     result[empty] = notFound
        // }
    }
    return result 
}

const data = require("../data.js")
// console.log(filterData4(data))
module.exports = filterData4(data)

// company nya Pelangi atau Intel, dan warna mata nya hijau