const express = require("express");
const app = express();

const port = 3000;
const allData = require("./data/data");
const bananaLovers = require("./data/sortData/data1");
const ageOld = require("./data/sortData/data2");
const appleLovers = require("./data/sortData/data3");
const greenEyes = require("./data/sortData/data4");
const active = require("./data/sortData/data5");

app.set("view engine", "ejs"); // menggunakan view engine ejs
app.use(express.static("public")); //built in middleware

// Halaman Home
app.get("/", (req, res) => {
  res.render("index", {
    name: "Atha Ardisa",
    title: "Halaman Home",
  });
});

// Halaman About
app.get("/about", (req, res) => {
  res.render("about", {
    title: "Halaman About",
  });
});

// membuka semua data
app.get("/dataPers", (req, res) => {
  res.render("dataPers", {
    title: "Halaman Data",
    data: allData,
  });
});

// age nya dibawah 30 tahun dan favorit buah nya pisang
app.get("/dataPers/1", (req, res) => {
  res.render("dataPers", {
    title: "Banana Lovers",
    data: bananaLovers,
  });
});

// gender nya female atau company nya FSW4 dan age nya diatas 30 tahun
app.get("/dataPers/2", (req, res) => {
  res.render("dataPers", {
    title: "Age More Than 30",
    data: ageOld,
  });
});

// warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel
app.get("/dataPers/3", (req, res) => {
  res.render("dataPers", {
    title: "Apple Lovers",
    data: appleLovers,
  });
});

// company nya Pelangi atau Intel, dan warna mata nya hijau
app.get("/dataPers/4", (req, res) => {
  res.render("dataPers", {
    title: "Green Eyes",
    data: greenEyes,
  });
});

// registered di bawah tahun 2016 dan masih active(true)
app.get("/dataPers/5", (req, res) => {
  res.render("dataPers", {
    title: "Active",
    data: active,
  });
});

// LATIHAN REQ PARAMS
// app.get("/product/:id", (req, res) => {
//   res.send(`Product ID = ${req.params.id} <br>
//     Category = ${req.query.category}`);
// });

// Error Handler
app.use("/", (req, res) => {
  res.status(404);
  res.render("404");
});

// server
app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port}`);
});
